package co.simplon.cda.vehicleconfiguratorapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehicleConfiguratorApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VehicleConfiguratorApiApplication.class, args);
	}

}
